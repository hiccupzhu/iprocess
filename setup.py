#!/usr/bin/env python
#coding=utf-8

import os
import sys

sources = '''
cprogramproxy.py
cprogram.py
cxmlrpcserver.py
daemon.py
http2.py
iprocessctrl.py
iprocessd.py
iprocesshttp.py
processdconfig.py
programconfig.py
''';

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "Too few arguments."
        print "  e.g.>"
        print "       python setup.py install"
        sys.exit(0);
    
    if os.geteuid() != 0:
        print "Permission denied, Please use root try again."
        sys.exit(0);
    
    src_install_dir = "/etc/iprocess/src"
    www_install_dir = "/etc/itvencoder/www";
    
    os.system('mkdir -p /var/log/iprocess');
    
    os.system('mkdir -p %s' % (src_install_dir));
    os.system('mkdir -p %s' % (www_install_dir));
    os.system('mkdir -p %s/cgi' % (www_install_dir));
    
    for src in sources.strip().split():
        src = os.getcwd() + "/" + src;
        os.system('install %s %s' % (src, src_install_dir))
        
    os.system('install %s /etc/itvencoder' % (os.getcwd()+"/iprocessd.ini"));
    os.system('install %s /usr/bin/itvshutdown' % (os.getcwd()+"/itvshutdown.sh"));    
    
    os.system('cp -r %s %s' % (os.getcwd() + "/www/*", www_install_dir))
    os.system('ln -sf %s/iprocessd.py    /usr/bin/iprocessd' % (src_install_dir))
    os.system('ln -sf %s/iprocessctrl.py /usr/bin/iprocessctrl' % (src_install_dir))
    
    os.system('ln -sf %s/iprocessd.py    %s/cgi/iprocessd'    % (src_install_dir, www_install_dir))
    os.system('ln -sf %s/iprocessctrl.py %s/cgi/iprocessctrl' % (src_install_dir, www_install_dir))
    
    
    
    



