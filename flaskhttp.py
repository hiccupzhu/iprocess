#!/usr/bin/env python
#coding=utf-8

__version__ = "0.3"

import os
import sys
import time
import threading
import re
import meld3
import subprocess
import logging
import flask
import processdconfig as pdc

logger = logging.getLogger('iprocessd.http');
app = flask.Flask(__name__);





def set_root_path(path):
    app.root_path = path;


@app.route('/')
@app.route('/index.html')
def index():
    html = flask.render_template('index.html');
    root = meld3.parse_htmlstring(html);
    data = pdc.get_programs_status_data()
      
    iterator = root.findmeld('tr').repeat(data)
    for element, item in iterator:
        element.findmeld('status_text').content(item['status'])
        element.findmeld('live_time').content(item['live_time'])
        element.findmeld('info_text').content(item['info'])
        element.findmeld('name_anchor').content(item['name'])
#             element.findmeld('actionitem_anchor').content(item['action'])
        element.findmeld('actionitem_anchor').attributes(value = item['action'])
        element.findmeld('actionitem_anchor').attributes(href = '/cgi/iprocessctrl/%s/%s' % (item['action'].lower(), item['name']))
        element.findmeld('edititem_anchor').attributes(href = '/configure/chname=%s' % (item['name']))
        element.findmeld('edititem_anchor').attributes(name = item['name']);
    
    return root.write_xhtmlstring()

@app.route('/configure/chname=<name>')
def get_config_with_name(name):
    print "--------------------------------------------------"
    print "@@@@@@@@@@@@@ chname=", name
#     print flask.request.args.get('chname', '')
    print "___________________________________________________"
    programs = pdc.get_programs();
    if programs.has_key(name):
        prog = programs[name];
        if prog.config != None:
            return prog.config.variable_jsonstring();
        else:
            return '<p>The program "%s" NOT itvencoder !!!</p>' % (name);
    else:
        return '<p>Channel "%s" NOT exist !!!</p>' % (name);


@app.route('/cgi/<program>/<action>/<name>')
def cgi_function(program, action, name):
    command = '%s %s %s' % (program, action, name)
    logger.info('CGI: "%s" reading output...' % (command));
    
    pipe = subprocess.Popen(command.split(), stdin = subprocess.PIPE,
                         stdout = subprocess.PIPE,
                         stderr = subprocess.STDOUT,
                         shell = False);
    
    data = '<html><h3>"%s" return: </h3> <p>' % (command);
    for line in pipe.stdout.readlines():
        data += '<p>%s</p>' % (line);
    
    pipe.wait();
    data += '</p></html>'
    
    logger.info('CGI: "%s" quit.' % (command));
    
    return data;
    

@app.route('/version')
def get_version():
    html = "";
    for line in file('/etc/itvencoder-release', 'r'):
        html += '<p>%s</p>' % (line);
    
    return html;

class iProcessHTTP():
    def __init__(self, host = '0.0.0.0', port = 8080, debug = True):
        self.thread = None;
        self.host = host;
        self.port = port;
        self.debug_flag = debug;
    
    def start(self):
        self.thread = threading.Thread(target=self.run)
        self.thread.setDaemon(True);
        self.thread.start();
    
    def run(self):
        app.run(self.host, self.port, self.debug_flag, use_reloader=False, threaded=True);
    
    def stop(self):
        logger.info('The "HTTPServer" will be quit after program exit.');




if __name__ == "__main__":
    PORT_NUMBER = 8080
    try:
        set_root_path('/work/code/iprocess/www');
#         os.chdir("/work/code/iprocess/ui");
        #Create a web server and define the handler to manage the
        #incoming request
        http = iProcessHTTP();
        http.start();
#         http.run('0.0.0.0', 5000, debug = True);
        
        while True:
            time.sleep(0.2);

    except KeyboardInterrupt:
        print '^C received, shutting down the web server'

    
    print "Program exit ... "









