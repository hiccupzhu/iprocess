import daemon
import logging
import processdconfig as pdc

class CProgramProxy:
    def __init__(self):
        self.logger = logging.getLogger('iprocessd.pproxy');
        
    def list(self):
        programs = pdc.get_programs();
        return programs.keys();
    
    def status(self):
        programs = pdc.get_programs();
        ret = {};
        for key, prog in programs.items():
            ret[key] = 'Running' if prog.isAlive() else 'Stopped';
        return ret;
    
    def start(self, name):
        programs = pdc.get_programs();
        if programs.has_key(name):
            self.logger.warning('Start program "%s" ...' % (name));
            prog = programs[name];
            prog.start();
            self.logger.warning('Start program "%s" successful' % (name));
        else:
            self.logger.warning('Start program "%s" not exist !' % (name));
        return 0;
    
    def kill(self, name):
        programs = pdc.get_programs();
        if programs.has_key(name):
            prog = programs[name];
            prog.kill();
            self.logger.warning('Kill program "%s"!' % (name));
        else:
            self.logger.warning('Kill program "%s" not exist !' % (name));
        return 0;
    
    def stop(self, name):
        programs = pdc.get_programs();
        if programs.has_key(name):
            prog = programs[name];
            prog.stop(True);
            self.logger.warning('Stop program "%s"!' % (name));
        else:
            self.logger.warning('Stop program "%s" not exist !' % (name));
        return 0;
    
    def shutdown(self):
        programs = pdc.get_programs();
        for (name, prog) in programs.items():
            prog.stop(True);
            self.logger.warning('Shutdown stop program "%s"!' % (name));
        daemon.shutdown();
        self.logger.warning('Shutdown daemon!');
        return 0;
    
    def add_channel(self, name):
        self.logger.info('Add channel "%s" ...' % (name));
        ret = pdc.add_channel(name);
        self.logger.info('Add channel %d:%s', ret)
        return ret;
    
    def del_channel(self, name):
        self.stop(name);
        self.logger.info('Del channel "%s" ...' % (name));
        ret = pdc.del_channel(name);
        self.logger.info('Del channel %d:%s', ret)
        return ret;
        