window.onload = function(){
};

function actionClick(obj){
   //action = obj.value.toLowerCase();
   
   command = obj.getAttribute('href');
   
   $.ajax({
        type: "GET",
        url: command,
        success:function(html){
           //$("#msgdisplay").html(html);
           
           window.location.reload();
        }
   });
}


function trim(str){   
    return str.replace(/^(\s|\xA0)+|(\s|\xA0)+$/g, '');   
}  

function packet_type_select(packname, obj){
    var str = obj.type;
    var def_str = obj.context;
	str = str.replace('[', '').replace(']', '');
	var list = str.split(',');
	var html = '<select id="' + obj.name + '" name="' + packname + '">';
	for(var i = 0; i < list.length; i++){
	    if( trim(list[i]) == def_str){
	        html += '<option value="' + i + '" selected>' + list[i] + '</option>';
	    }else{
		    html += '<option value="' + i + '">' + list[i] + '</option>';
		}
	}
	html += "</select>";
	
	// console.info(html);
	
	return html;
}

function packet_cells_td(packname, obj){
    var data = '<td>';
    
    data += ' <label>' + obj.name + ':</label>';
    
    var re = /^\[[\w ,]+\]$/;
    if(re.test(obj.type)){
        data += packet_type_select(packname, obj);
    }else if(obj.type == 'option'){
        if(obj.context.toLowerCase() == "yes"){
            data += '<input id="' + obj.name + '" name="' + packname + '" type="checkbox" checked="checked" />';
        }else{
            data += '<input id="' + obj.name + '" name="' + packname + '" type="checkbox" />';
        }
    }else{
        data += '<input id="' + obj.name + '" name="' + packname + '" type="text" value=' + obj.context + ' />';
    }
    data += '</td>';
    
    return data;
}

function packet_cells_tr(packname, obj){
    var html = "";
    html += "<tr>"
    for(var j = 0; j < obj.length; j++){
        if(j % 2 == 0){
            html += "</tr><tr>"
        }
        html += packet_cells_td(packname, obj[j]);
    }
    html += "</tr>"
    
    return html;
}

function packet_sources(src){
    var html = '<tr> <td><table border=0 cellspacing="0">';
    html += packet_cells_tr('source', src);
    html += "</table></td></tr>";
    return html;
}

function packet_encoders(encoders){
    var html = "";
    // console.info("##############");
    for(var i = 0; i < encoders.length; i++){
        encoder = encoders[i];
        html += '<tr><td><table>';    
        html += packet_cells_tr('encoder' + i, encoder);
        html += '</table></td></tr>';
    }
    
    return html;
}

function packet_server_section(server){
    var html = '<tr><td><table>';
    html += packet_cells_tr('server', server);
    html += '</table></td></tr>'
    
    return html;
}

function ganarateConfTemplate(chname, json_string){
	//var json_string = '{"source": [{"type": "string", "name": "udpsrc uri", "context": "udp://127.0.0.1:20000"}, {"type": "[0, 1, 2, 3, 4]", "name": "deinterlace-method", "context": "4"}, {"type": "[0, 1, 2]", "name": "deinterlace-fields", "context": "1"}], "encoders": [[{"type": "number", "name": "width", "context": "1024"}, {"type": "number", "name": "height", "context": "576"}, {"type": "[TRUE, FALSE]", "name": "keep-aspect-ratio", "context": "FALSE"}, {"type": "number", "name": "bitrate", "context": "1200"}, {"type": "number", "name": "b frames", "context": "3"}], [{"type": "number", "name": "width", "context": "1024"}, {"type": "number", "name": "height", "context": "576"}, {"type": "[TRUE, FALSE]", "name": "keep-aspect-ratio", "context": "FALSE"}, {"type": "number", "name": "bitrate", "context": "1200"}, {"type": "number", "name": "b frames", "context": "3"}]]}'
	// var json_string = '{"source": [{"type": "string", "name": "udpsrc uri", "context": "udp://127.0.0.1:20000"}, {"type": "[0, 1, 2, 3, 4]", "name": "deinterlace-method", "context": "4"}, {"type": "[0, 1, 2]", "name": "deinterlace-fields", "context": "1"}, {"type": "string", "name": "cachesize", "context": "0x40000"}, {"type": "[mpeg2dec, h264dec]", "name": "video decoder", "context": "h264dec"}, {"type": "[mad, ffdec_ac3, ffdec_eac3, ffdec_aac]", "name": "audio decoder", "context": "mad"}, {"type": "option", "name": "select audio0", "context": "yes"}, {"type": "option", "name": "select subtitle0", "context": "yes"}], "encoders": [[{"type": "number", "name": "width", "context": "1024"}, {"type": "number", "name": "height", "context": "576"}, {"type": "[TRUE, FALSE]", "name": "keep-aspect-ratio", "context": "FALSE"}, {"type": "number", "name": "bitrate", "context": "1200"}, {"type": "number", "name": "b frames", "context": "3"}, {"type": "[baseline, main, high]", "name": "profile", "context": "high"}, {"type": "number", "name": "threads", "context": "12"}, {"type": "option", "name": "select subtitle0", "context": "yes"}, {"type": "option", "name": "select audio0", "context": "yes"}]]}';

	json = JSON.parse(json_string);
	//alert(json.source[0].type);
	
	var html = "<form><table cellspacing=0>";
	
	html += packet_server_section(json.server);
	html += packet_sources(json.source);
	html += packet_encoders(json.encoders);

	
	html += '</table>';
	html += '<input type="button" onclick="cancelClick(this)" value="Cancel"/>';
	html += '<input type="button" onclick="saveClick(this)" name="' + chname + '" value="Save"/>';
	
	html += "</form>";
	//alert(html);
	
	return html;
}

function editClick(obj){
    command = obj.getAttribute('href');
    chname = obj.getAttribute('name');
    
    console.info("##############chname=" + chname);
    
    $.ajax({
        type: "GET",
        url: command,
        success:function(data){
            // console.info(data);
            var html = "";
            if(data.charAt(0) == '{'){
                // console.info("##############");
                html = ganarateConfTemplate(chname, data);
            }else{
                // console.info("##############");
                html = data;
            }
            
            divDisplay("div_channel_info", html);
        }
   });
}

function objs_to_json(objs){
    var sections = {};
    
    for(var i = 0; i < objs.length; i++){
        obj = objs[i];
        
        var value = "";

        if(obj.type == 'select-one'){
            var index = obj.selectedIndex;
            // console.info('"' + obj.id + '"  selected="' + obj.options[index].text + '"');
            value = obj.options[index].text;
        }else if(obj.type == 'text'){
            // console.info('"' + obj.id + '"  text="' + obj.value + '"');
            value = obj.value;
        }else if(obj.type == 'checkbox'){
            // console.info('"' + obj.id + '"  option="' + obj.checked + '"');
            if(obj.checked){
                value = 'yes';
            }else{
                value = 'no';
            }
        }else{
            console.info('Unknown type="' + obj.type + '"');
        }
        
        sections[obj.id] = value;
    }
    
    return sections;
}

function get_div_jsonstring(){
    var channel = {};
    
    channel['server'] = objs_to_json(document.getElementsByName('server'));
    channel['source'] = objs_to_json(document.getElementsByName('source'));
    
    encoders = [];
    for(var i = 0; i < 10; i++){
        enc_name = "encoder" + i;
        xencoders = document.getElementsByName(enc_name);
        if(xencoders.length == 0){
            break;
        }
        encoders.push(objs_to_json(xencoders));
    }
    channel['encoders'] = encoders;
    
    return JSON.stringify(channel);
}

function saveClick(obj){
    var json_string = get_div_jsonstring();
    var url_string = '/setconfigure?' + obj.getAttribute('name');;
    
    $.ajax({
        type: "GET",
        url: url_string,
        data: json_string,
        success:function(html){
           //$("#msgdisplay").html(html);
            div = document.getElementById('div_channel_info');
            div.innerHTML = html;
        }
   });
}


function divDisplay(div_name, html){
    div = document.getElementById(div_name);
    
    div.style.display = "";
    div.innerHTML = html;

}

function cancelClick(obj){
	div = document.getElementById('div_channel_info');
	//hidden the DIV
    div.style.display = "none";
	div.innerHTML = '<p></p>';
}

