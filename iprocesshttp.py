#!/usr/bin/env python
#coding=utf-8

__version__ = "0.3"

import os
import sys
import time
import socket
import SocketServer
import mimetools
import threading
import re
import meld3
import urllib
import subprocess
import logging
import processdconfig as pdc


def _quote_html(html):
    return html.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")

def _unquote_html(html):
    tokens = (('&nbsp;', ' '),
              ('&amp;',  '&'),
              ('&lt;' ,  '<'),
              ('&gt;' ,  '>')
             );
    for t0, t1 in tokens:
        html = html.replace(t0, t1);
    
    return html;

class HTTPServer(SocketServer.TCPServer):
    allow_reuse_address = True    # Seems to make sense in testing environment

    def server_bind(self):
        """Override server_bind to store the server name."""
        logger = logging.getLogger('iprocessd.http');
        try:
            SocketServer.TCPServer.server_bind(self)
        except socket.error, e:
            logger.error('HTTP bind FAILED: %s' % (str(e)));
            sys.exit(0);
        
        host, port = self.socket.getsockname()[:2]
#         self.server_name = socket.getfqdn(host)
        self.server_port = port

class BaseHTTPRequestHandler(SocketServer.StreamRequestHandler):
    # The Python system version, truncated to its first component.
    sys_version = "Python/" + sys.version.split()[0]

    # The server software version.  You may want to override this.
    # The format is multiple whitespace-separated strings,
    # where each string is of the form name[/version].
    server_version = "BaseHTTP/" + __version__

    # The default request version.  This only affects responses up until
    # the point where the request line is parsed, so it mainly decides what
    # the client gets back when sending a malformed request line.
    # Most web servers default to HTTP 0.9, i.e. don't send a status line.
    default_request_version = "HTTP/0.9"

    def parse_request(self, requestline):
        self.command = None  # set in case of error on the first line
        self.request_version = version = self.default_request_version
        self.close_connection = 1
        
        logger = logging.getLogger("iprocessd.http");

        logger.debug(requestline);
#         print requestline;
        requestline = requestline.rstrip('\r\n')
        self.requestline = requestline
        words = requestline.split()
        if len(words) == 3:
            command, path, version = words
            if version[:5] != 'HTTP/':
                self.send_error(400, "Bad request version (%r)" % version)
                return False
            try:
                base_version_number = version.split('/', 1)[1]
                version_number = base_version_number.split(".")
                # RFC 2145 section 3.1 says there can be only one "." and
                #   - major and minor numbers MUST be treated as
                #      separate integers;
                #   - HTTP/2.4 is a lower version than HTTP/2.13, which in
                #      turn is lower than HTTP/12.3;
                #   - Leading zeros MUST be ignored by recipients.
                if len(version_number) != 2:
                    raise ValueError
                version_number = int(version_number[0]), int(version_number[1])
            except (ValueError, IndexError):
                self.send_error(400, "Bad request version (%r)" % version)
                return False
            if version_number >= (1, 1) and self.protocol_version >= "HTTP/1.1":
                self.close_connection = 0
            if version_number >= (2, 0):
                self.send_error(505,
                          "Invalid HTTP Version (%s)" % base_version_number)
                return False
        elif len(words) == 2:
            command, path = words
            self.close_connection = 1
            if command != 'GET':
                self.send_error(400,
                                "Bad HTTP/0.9 request type (%r)" % command)
                return False
        elif not words:
            return False
        else:
            self.send_error(400, "Bad request syntax (%r)" % requestline)
            return False
        self.command, self.path, self.request_version = command, path, version
        
        self.path = urllib.unquote(self.path);

        # Examine the headers and look for a Connection directive
        self.headers = self.MessageClass(self.rfile, 0)

#         print self.headers;

        conntype = self.headers.get('Connection', "")
        if conntype.lower() == 'close':
            self.close_connection = 1
        elif (conntype.lower() == 'keep-alive' and
              self.protocol_version >= "HTTP/1.1"):
            self.close_connection = 0
        return True

    def handle_one_request(self):
        try:
            self.raw_requestline = self.rfile.readline(65537)
            if len(self.raw_requestline) > 65536:
                self.requestline = ''
                self.request_version = ''
                self.command = ''
                self.send_error(414)
                return
            if not self.raw_requestline:
                self.close_connection = 1
                return
            if not self.parse_request(self.raw_requestline):
                # An error code has been sent, just exit
                return
            
            mname = 'do_' + self.command
            if not hasattr(self, mname):
                self.send_error(501, "Unsupported method (%r)" % self.command)
                return
            method = getattr(self, mname)
            method()
            self.wfile.flush() #actually send the response if not already done.
        except socket.timeout, e:
            #a read or a write timed out.  Discard this connection
            self.log_error("Request timed out: %r", e)
            self.close_connection = 1
            return

    def handle(self):
        """Handle multiple requests if necessary."""
        self.close_connection = 1

        self.handle_one_request()
        while not self.close_connection:
            self.handle_one_request()

    def send_error(self, code, message=None):
        try:
            short, long = self.responses[code]
        except KeyError:
            short, long = '???', '???'
        if message is None:
            message = short
        explain = long
        # using _quote_html to prevent Cross Site Scripting attacks (see bug #1100201)
        content = ("""\
            <head><title>Error response</title></head>
            <body>
            <h1>Error response</h1>
            <p>Error code %(code)d.
            <p>Message: %(message)s.
            <p>Error code explanation: %(code)s = %(explain)s.
            </body>
            """ % {'code': code, 'message': _quote_html(message), 'explain': explain})
        self.send_response(code, message)
        self.send_header("Content-Type", "text/html")
        self.send_header('Connection', 'close')
        self.end_headers()
        if self.command != 'HEAD' and code >= 200 and code not in (204, 304):
            self.wfile.write(content)

    def send_response(self, code, message=None):
        if message is None:
            if code in self.responses:
                message = self.responses[code][0]
            else:
                message = ''
        if self.request_version != 'HTTP/0.9':
            self.wfile.write("%s %d %s\r\n" %
                             (self.protocol_version, code, message))
            
        self.send_header('Server', self.version_string())
        self.send_header('Date', self.date_time_string())

    def send_header(self, keyword, value):
        """Send a MIME header."""
        if self.request_version != 'HTTP/0.9':
            self.wfile.write("%s: %s\r\n" % (keyword, value))

        if keyword.lower() == 'connection':
            if value.lower() == 'close':
                self.close_connection = 1
            elif value.lower() == 'keep-alive':
                self.close_connection = 0

    def end_headers(self):
        """Send the blank line ending the MIME headers."""
        if self.request_version != 'HTTP/0.9':
            self.wfile.write("\r\n")

    def version_string(self):
        """Return the server software version string."""
        return self.server_version + ' ' + self.sys_version

    def date_time_string(self, timestamp=None):
        """Return the current date and time formatted for a message header."""
        if timestamp is None:
            timestamp = time.time()
        year, month, day, hh, mm, ss, wd, y, z = time.gmtime(timestamp)
        s = "%s, %02d %3s %4d %02d:%02d:%02d GMT" % (
                self.weekdayname[wd],
                day, self.monthname[month], year,
                hh, mm, ss)
        return s

    def log_date_time_string(self):
        """Return the current time formatted for logging."""
        now = time.time()
        year, month, day, hh, mm, ss, x, y, z = time.localtime(now)
        s = "%02d/%3s/%04d %02d:%02d:%02d" % (
                day, self.monthname[month], year, hh, mm, ss)
        return s

    weekdayname = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']

    monthname = [None,
                 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

    def address_string(self):
        host, port = self.client_address[:2]
        return socket.getfqdn(host)

    # Essentially static class variables

    # The version of the HTTP protocol we support.
    # Set this to HTTP/1.1 to enable automatic keepalive
    protocol_version = "HTTP/1.0"

    # The Message-like class used to parse headers
    MessageClass = mimetools.Message

    # Table mapping response codes to messages; entries have the
    # form {code: (shortmessage, longmessage)}.
    # See RFC 2616.
    responses = {
        100: ('Continue', 'Request received, please continue'),
        101: ('Switching Protocols',
              'Switching to new protocol; obey Upgrade header'),

        200: ('OK', 'Request fulfilled, document follows'),
        201: ('Created', 'Document created, URL follows'),
        202: ('Accepted',
              'Request accepted, processing continues off-line'),
        203: ('Non-Authoritative Information', 'Request fulfilled from cache'),
        204: ('No Content', 'Request fulfilled, nothing follows'),
        205: ('Reset Content', 'Clear input form for further input.'),
        206: ('Partial Content', 'Partial content follows.'),

        300: ('Multiple Choices',
              'Object has several resources -- see URI list'),
        301: ('Moved Permanently', 'Object moved permanently -- see URI list'),
        302: ('Found', 'Object moved temporarily -- see URI list'),
        303: ('See Other', 'Object moved -- see Method and URL list'),
        304: ('Not Modified',
              'Document has not changed since given time'),
        305: ('Use Proxy',
              'You must use proxy specified in Location to access this '
              'resource.'),
        307: ('Temporary Redirect',
              'Object moved temporarily -- see URI list'),

        400: ('Bad Request',
              'Bad request syntax or unsupported method'),
        401: ('Unauthorized',
              'No permission -- see authorization schemes'),
        402: ('Payment Required',
              'No payment -- see charging schemes'),
        403: ('Forbidden',
              'Request forbidden -- authorization will not help'),
        404: ('Not Found', 'Nothing matches the given URI'),
        405: ('Method Not Allowed',
              'Specified method is invalid for this resource.'),
        406: ('Not Acceptable', 'URI not available in preferred format.'),
        407: ('Proxy Authentication Required', 'You must authenticate with '
              'this proxy before proceeding.'),
        408: ('Request Timeout', 'Request timed out; try again later.'),
        409: ('Conflict', 'Request conflict.'),
        410: ('Gone',
              'URI no longer exists and has been permanently removed.'),
        411: ('Length Required', 'Client must specify Content-Length.'),
        412: ('Precondition Failed', 'Precondition in headers is false.'),
        413: ('Request Entity Too Large', 'Entity is too large.'),
        414: ('Request-URI Too Long', 'URI is too long.'),
        415: ('Unsupported Media Type', 'Entity body in unsupported format.'),
        416: ('Requested Range Not Satisfiable',
              'Cannot satisfy request range.'),
        417: ('Expectation Failed',
              'Expect condition could not be satisfied.'),

        500: ('Internal Server Error', 'Server got itself in trouble'),
        501: ('Not Implemented',
              'Server does not support this operation'),
        502: ('Bad Gateway', 'Invalid responses from another server/proxy.'),
        503: ('Service Unavailable',
              'The server cannot process the request due to a high load'),
        504: ('Gateway Timeout',
              'The gateway server did not receive a timely response'),
        505: ('HTTP Version Not Supported', 'Cannot fulfill request.'),
        }


#This class will handles any incoming request from
#the browser 
class iProcessHTTPHandler(BaseHTTPRequestHandler):
    def __init__(self, request, client_address, server):
        self.init_mimes();
        self.logger = logging.getLogger("iprocessd.http");
        BaseHTTPRequestHandler.__init__(self, request, client_address, server);
        
        
        
    def do_GET(self):
        self.logger.debug("do_Get %s" % (self.path));
#         print "### do_Get %s" % (self.path)
        HTTP_ROOT = pdc.get_http_root();

        if self.path in ("/", "/index.html"):
            self.logger.info("send the index.html ...");
            self.send_index(HTTP_ROOT + "/index.html");
        elif os.path.exists(HTTP_ROOT + "/" + self.path):
            self.logger.debug("send %s ..." % (self.path));
            self.send_file (HTTP_ROOT + self.path);
        elif self.path.find('/configure?') == 0:
            name = self.path[len('/configure?'):];
            self.send_response(200);
            self.send_header("Content-Type", "text/html")
            self.end_headers()
            
            programs = pdc.get_programs();
            if programs.has_key(name):
                prog = programs[name];
                if prog.config != None:
                    self.wfile.write(prog.config.variable_jsonstring());
                else:
                    self.wfile.write('<p>The program "%s" NOT itvencoder !!!</p>' % (name));
            else:
                self.wfile.write('<p>Channel "%s" NOT exist !!!</p>' % (name));
        elif self.path.find('/setconfigure?') == 0:
            path = self.path;
#             self.logger.info('GET:' + path);
            name = self.path[len('/setconfigure?'):path.find('&')];
            
            json_string = path[path.find('&') + 1:];
            
            self.logger.debug('name=%s json_string:%s' %  (name, json_string));
            
            self.send_response(200);
            self.send_header("Content-Type", "text/html")
            self.end_headers();
            
            programs = pdc.get_programs();
            if programs.has_key(name):
                prog = programs[name];
                if prog.config != None:
                    if prog.config.set_variable_jsonstring(json_string) == 0:
                        self.wfile.write('<p>Set program "%s" config successful!!!</p>' % (name));
                    else:
                        self.wfile.write('<p>Set program "%s" config FAILED!!!</p>' % (name));
                else:
                    self.wfile.write('<p>The program "%s" NOT itvencoder !!!</p>' % (name));
            else:
                self.wfile.write('<p>Channel "%s" NOT exist !!!</p>' % (name));
        elif self.path.find("/cgi/") == 0:
            self.logger.info("CGI: %s ..." % (self.path));

            command = HTTP_ROOT + self.path
            self.logger.info("CGI: command:%s" % (command));
            pipe = subprocess.Popen(command.split(), stdin = subprocess.PIPE,
                         stdout = subprocess.PIPE,
                         stderr = subprocess.STDOUT,
                         shell = False);
            
#             self.send_response(200);
#             self.wfile.write("Content-Type:text/xml\r\n")
            
            self.logger.info("CGI: %s reading output..." % (self.path));
            
            data = '<html><h3>"%s" return: </h3> <p>' % (self.path);
            for line in pipe.stdout.readlines():
                data += '<p>%s</p>' % (line);
            
            pipe.wait();
            data += '</p></html>'
            
            self.wfile.write(data)
            
            self.logger.info('CGI: "%s" quit.' % (self.path));
            
        else:
            param = self.path.split('?');
            if len(param) != 2:
                print "%s Not Find!!!" % (self.path.decode);
                self.send_error(404);
            else:
                params = param[1].split('&');
                print params;
                self.send_response(200);
                self.send_header("Content-Type", "text/html")
                self.end_headers()
                self.wfile.write(str(params));

        self.logger.debug("### do_Get leaving");

        return
    
    def do_POST(self):
        self.logger.info("POST %s" % (self.path));
        
        line = self.requestline;
        print " Requestline=", line
        
        pass;
    
    def send_index(self, file_path):
        print "#####", file_path
        
        self.send_response(200)
        self.send_header('Content-type', "text/html")
        self.end_headers()
        
        html = file(file_path, "r").read();
        root = meld3.parse_htmlstring(html);
        data = pdc.get_programs_status_data()
          
        iterator = root.findmeld('tr').repeat(data)
        for element, item in iterator:
            element.findmeld('status_text').content(item['status'])
            element.findmeld('info_text').content(item['info'])
            element.findmeld('name_anchor').content(item['name'])
#             element.findmeld('actionitem_anchor').content(item['action'])
            element.findmeld('actionitem_anchor').attributes(value = item['action'])
            element.findmeld('actionitem_anchor').attributes(href = '/cgi/iprocessctrl %s %s' % (item['action'].lower(), item['name']))
            element.findmeld('edititem_anchor').attributes(href = '/configure?%s' % (item['name']))
            element.findmeld('edititem_anchor').attributes(name = item['name']);
        
        self.wfile.write(root.write_xhtmlstring());

    def send_file(self, file_path):
        ext = os.path.splitext(file_path)[1][1:];
        ctx_type = self.mimes[ext];

        self.send_response(200)
        self.send_header('Content-type', ctx_type)
        self.end_headers()

        # Send the html message
        data = file(file_path, "r").read();
        if ctx_type == "text/html":
            data = _unquote_html(data);
   
        self.wfile.write(data);
    
    def init_mimes(self):
        mm = {"application/ogg":      " ogg",
                 "application/pdf":      " pdf",
                 "application/xml":      " xsl xml",
                 "application/xml-dtd":  " dtd",
                 "application/xslt+xml": " xslt",
                 "application/zip":      " zip",
                 "audio/mpeg":           " mp2 mp3 mpga",
                 "image/gif":            " gif",
                 "image/jpeg":           " jpeg jpe jpg",
                 "image/png":            " png",
                 "text/css":             " css",
                 "text/html":            " html htm",
                 "text/javascript":      " js",
                 "text/plain":           " txt asc",
                 "video/mpeg":           " mpeg mpe mpg",
                 "video/quicktime":      " qt mov",
                 "video/x-msvideo":      " avi",
                 };
        mimes = {};
        for key, value in mm.items():
            words = value.split();
            for w in words:
                mimes[w] = key
            
        self.mimes = mimes;
        

class iProcessHTTP(threading.Thread):
    def __init__(self, port):
        threading.Thread.__init__(self);
        self.server = HTTPServer(('', port), iProcessHTTPHandler)
#         print 'Started httpserver on port ' , self.server.server_address
    
    def start(self):
        
        threading.Thread.start(self);
    
    def run(self):
        print 'Started httpserver on port ' , self.server.server_address
        self.server.serve_forever();
    
    def stop(self):
        self.server.shutdown();
        

if __name__ == "__main__":
    PORT_NUMBER = 8080
    try:
#         os.chdir("/work/code/iprocess/ui");
        #Create a web server and define the handler to manage the
        #incoming request
        server = HTTPServer(('', PORT_NUMBER), iProcessHTTPHandler)
        print 'Started httpserver on port ' , PORT_NUMBER

        #Wait forever for incoming htto requests
        server.serve_forever()
        

    except KeyboardInterrupt:
        print '^C received, shutting down the web server'
        server.socket.close()

    
    server.shutdown();
