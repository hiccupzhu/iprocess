#!/usr/bin/env python
#coding=utf-8

import os
import sys
import re
import subprocess


class Process:
    def __init__(self, (UID, PID, PPID, C, STIME, TTY, TIME, CMD)):
        self.UID = UID;
        self.PID = long(PID);
        self.PPID = long(PPID);
        self.C = long(C);
        self.STIME = STIME;
        self.TTY = TTY;
        self.TIME = TIME;
        self.CMD = CMD;
        
    def __str__(self):
        return "%s %d %d %d %s %s %s %s" % (self.UID, self.PID, self.PPID, self.C, self.STIME, self.TTY, self.TIME, self.CMD)
    
    def kill(self):
        ret = os.system('kill %d' % (self.PID));
        if ret == 0:
            print 'Kill %d successfull.' % (self.PID);
        else:
            print 'Kill %d failed.' % (self.PID);

def parse_process_info(text):
    regexp_char = '\w\/\s\[\],:\.-_=';
    regexp = '^(\w+)\s+([0-9]+)\s+([0-9]+)\s+([0-9]+)\s+([\w:]+)\s+([\w/\?]+)\s+([0-9:]+)\s+([%s]+)' % (regexp_char);
    m = re.match(regexp, text);
    if m:
        return m.groups();
    else:
        return None;
    
    
def get_all_process():
    command = 'ps -ef'
    pipe = subprocess.Popen(command.split(), stdin = subprocess.PIPE,
                         stdout = subprocess.PIPE,
                         stderr = subprocess.STDOUT,
                         shell = False);
    processes = [];
    
    while True:
        log = pipe.stdout.readline();
        if log == "":
            break;
        ret = parse_process_info(log.strip());
        if ret != None:
            processes.append(Process(ret));
        else:
            continue;
    
    pipe.wait();
    
    
    ######################
#     for p in processes:
#         print p;
    
    return processes;


    
def get_process_regexp(regexp):
    processes = get_all_process();
#     print len(processes)
    for p in processes:
#         print p.CMD
        m = re.search(regexp, p.CMD);
        if m:
            return p;
    return None;

def get_process_tree_from_parentID(id):
    processes = [];
    
    for p in get_all_process():
        if p.PID == id:
            processes.insert(0, p);
        elif p.PPID == id:
            processes.append(p);
    
    return processes;



if __name__ == "__main__":
   
    
#     sys.path.append(os.getcwd() + '/psutil')
    p = get_process_regexp('iprocessd\.py');
    ps = [];
    if p:
        ps = get_process_tree_from_parentID(p.PID);
    
    for p in ps:
        print p;
    
    if len(ps) >= 2:
        ps[1].kill();
    
    