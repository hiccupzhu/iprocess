import sys, os, time
import atexit
from signal import SIGTERM
import logging

DAEMON_PID_FILE = '/tmp/iprocessd.pid';
IS_DAEMON = False;

def get_pid_from_file(filename):
    '''
    return -1: the file not exist
    '''
    pid = -1;
    if os.path.exists(filename):
        fp = open(filename, "r");
        pid = int(fp.read().strip());
        fp.close();
    else:
        pid = -1;
    return pid;

def shutdown():
    """
    Stop the daemon
    """
    logger = logging.getLogger("iprocessd.daemon");
    
    logger.info("Check iprocessd pid-file ...")
    # Get the pid from the pidfile
    pid = get_pid_from_file(DAEMON_PID_FILE);

    if pid == -1:
        message = "pidfile %s does not exist. Daemon not running?\n"
        logger.warning(message % DAEMON_PID_FILE)
        return # not an error in a restart

    logger.info("Kill iprocessd ...")
    # Try killing the daemon process    
    try:
        while 1:
            global IS_DAEMON
            IS_DAEMON = False;
            os.kill(pid, SIGTERM)
            time.sleep(0.1)
    except OSError, err:
        logger.info("Shutdown iprocessd successful.")


def del_pidfile():
    os.system('rm -f %s' % (DAEMON_PID_FILE));

def daemonize(stdin, stdout, stderr):    
    try: 
        pid = os.fork() 
        if pid > 0:
            # exit first parent
            sys.exit(0) 
    except OSError, e: 
        sys.stderr.write("fork #1 failed: %d (%s)\n" % (e.errno, e.strerror))
        sys.exit(1)

    # decouple from parent environment
    os.chdir("/") 
    os.setsid() 
    os.umask(0)
    
    # do second fork
    try: 
        pid = os.fork() 
        if pid > 0:
            # exit from second parent
            sys.exit(0) 
    except OSError, e: 
        sys.stderr.write("fork #2 failed: %d (%s)\n" % (e.errno, e.strerror))
        sys.exit(1) 
    
    logger = logging.getLogger("iprocessd.daemon");
    
    
#     atexit.register(del_pidfile);

    # redirect standard file descriptors
    sys.stdout.flush()
    sys.stderr.flush()
    si = file(stdin, 'r')
    so = file(stdout, 'w')
    se = file(stderr, 'w', 0)
    os.dup2(si.fileno(), sys.stdin.fileno())
    os.dup2(so.fileno(), sys.stdout.fileno())
    os.dup2(se.fileno(), sys.stderr.fileno())
    
    logger.info("iProcessd started.");
    global IS_DAEMON
    IS_DAEMON = True;


def write_pidfile():
    if IS_DAEMON:
        file(DAEMON_PID_FILE,'w').write("%d\n" % os.getpid())
