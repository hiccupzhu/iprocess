import sys
import threading
import logging
import re
import SimpleXMLRPCServer as srpc
import BaseHTTPServer
import traceback
import SocketServer
import socket
try:
    import fcntl
except ImportError:
    fcntl = None



class CXMLRPCServer():
    def __init__(self, host = '0.0.0.0', port = 1122):
        self.logger = logging.getLogger('iprocessd.rpcsvr');
        self.host = host;
        self.port = port;
        self.server = SimpleXMLRPCServer((host, port));
        
    def __del__(self):
        self.logger.info('RPCServer eixting ... ');
        self.stop();
    
    def start(self):
        self.logger.info('RPCServer starting host="%s" port=%d ...' % (self.host, self.port))
        
        if self.server == None:
            self.server = SimpleXMLRPCServer((self.host, self.port));
        
        self.thread = threading.Thread(target=self.run)
        self.thread.setDaemon(True);
        self.thread.start();
        
    def run(self):
        if self.server:
            self.logger.info('RPCServer is running ...');
            self.server.serve_forever();
        else:
            self.logger.error('RPCServer is not created !!!');
        
        
    def stop(self):
        if self.server:
            self.logger.info('RPCServer stopping ...');
            self.server.shutdown();
#             self.server.server_close();
#             self.server = None;
            self.logger.info('RPCServer stopped');
        else:
            self.logger.warn('RPCServer is not created !!!');
        
    def register_instance(self, instance, allow_dotted_names=False):
        if self.server:
            self.server.register_instance(instance, allow_dotted_names);
        else:
            self.logger.warn('RPCServer is not created !!!');
        
    def register_function(self, function, name=None):
        if self.server:
            self.server.register_function(function, name)
        else:
            self.logger.warn('RPCServer is not created !!!');









'''Copy from the system
'''
class TCPServer(SocketServer.BaseServer):

    """Base class for various socket-based server classes.

    Defaults to synchronous IP stream (i.e., TCP).

    Methods for the caller:

    - __init__(server_address, RequestHandlerClass, bind_and_activate=True)
    - serve_forever(poll_interval=0.5)
    - shutdown()
    - handle_request()  # if you don't use serve_forever()
    - fileno() -> int   # for select()

    Methods that may be overridden:

    - server_bind()
    - server_activate()
    - get_request() -> request, client_address
    - handle_timeout()
    - verify_request(request, client_address)
    - process_request(request, client_address)
    - shutdown_request(request)
    - close_request(request)
    - handle_error()

    Methods for derived classes:

    - finish_request(request, client_address)

    Class variables that may be overridden by derived classes or
    instances:

    - timeout
    - address_family
    - socket_type
    - request_queue_size (only for stream sockets)
    - allow_reuse_address

    Instance variables:

    - server_address
    - RequestHandlerClass
    - socket

    """

    address_family = socket.AF_INET

    socket_type = socket.SOCK_STREAM

    request_queue_size = 5

    allow_reuse_address = False

    def __init__(self, server_address, RequestHandlerClass, bind_and_activate=True):
        """Constructor.  May be extended, do not override."""
        SocketServer.BaseServer.__init__(self, server_address, RequestHandlerClass)
        self.socket = socket.socket(self.address_family,
                                    self.socket_type)
        if bind_and_activate:
            self.server_bind()
            self.server_activate()

    def server_bind(self):
        """Called by constructor to bind the socket.

        May be overridden.

        """
#         self.server_address = ('0.0.0.0', 1123);
        if self.allow_reuse_address:
            self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind(self.server_address)
        self.server_address = self.socket.getsockname()

    def server_activate(self):
        """Called by constructor to activate the server.

        May be overridden.

        """
        self.socket.listen(self.request_queue_size)

    def server_close(self):
        """Called to clean-up the server.

        May be overridden.

        """
        self.socket.close()

    def fileno(self):
        """Return socket file number.

        Interface required by select().

        """
        return self.socket.fileno()

    def get_request(self):
        """Get the request and client address from the socket.

        May be overridden.

        """
        return self.socket.accept()

    def shutdown_request(self, request):
        """Called to shutdown and close an individual request."""
        try:
            #explicitly shutdown.  socket.close() merely releases
            #the socket and waits for GC to perform the actual close.
            request.shutdown(socket.SHUT_WR)
        except socket.error:
            pass #some platforms may raise ENOTCONN here
        self.close_request(request)

    def close_request(self, request):
        """Called to clean up an individual request."""
        request.close()



class SimpleXMLRPCServer(TCPServer,
                         srpc.SimpleXMLRPCDispatcher):
    allow_reuse_address = True;

    # Warning: this is for debugging purposes only! Never set this to True in
    # production code, as will be sending out sensitive information (exception
    # and stack trace details) when exceptions are raised inside
    # SimpleXMLRPCRequestHandler.do_POST
    _send_traceback_header = False

    def __init__(self, addr, requestHandler=srpc.SimpleXMLRPCRequestHandler,
                 logRequests=True, allow_none=False, encoding=None, bind_and_activate=True):
        self.logRequests = logRequests

        srpc.SimpleXMLRPCDispatcher.__init__(self, allow_none, encoding)
        TCPServer.__init__(self, addr, requestHandler, bind_and_activate)

        # [Bug #1222790] If possible, set close-on-exec flag; if a
        # method spawns a subprocess, the subprocess shouldn't have
        # the listening socket open.
        if fcntl is not None and hasattr(fcntl, 'FD_CLOEXEC'):
            flags = fcntl.fcntl(self.fileno(), fcntl.F_GETFD)
            flags |= fcntl.FD_CLOEXEC
            fcntl.fcntl(self.fileno(), fcntl.F_SETFD, flags)


