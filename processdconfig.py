import os
import ConfigParser
import re
import sys
import time
import logging
from cprogram import CProgram


rpc_port = 0;
    
http_port = 0;
http_ip = "";
http_root = "";
conf_filename = "";
config = None;
programs = {};
logfile = "";
logger = logging.getLogger('iprocessd.config');




def init(conf_file):
    global logfile, conf_filename, config
    
    conf_filename = conf_file;
    config = ConfigParser.ConfigParser();
    config.read(conf_filename);
    
    logfile = config.get("iprocessd", "logfile");
    
def string_to_bool(value):
    if value.lower() == "false":
        return False;
    elif value.lower() == "true":
        return True;
    else:
        return False;
    
def create_programs_with_section(section):
    pname = section[len("program:"):]
    prog = CProgram(pname);
    for (name, value) in config.items(section):
        if name == "command":
            prog.set_command(value);
        elif name == "autostart":
            prog.autostart = string_to_bool(value);
        elif name == "autorestart":
            prog.autorestart = string_to_bool(value);
        elif name == "startsecs":
            prog.startsecs = int(value);
        elif name == "logfile":
            prog.logfile = value;
        elif name == "logfile_maxbytes":
            m = re.match("^([0-9]+)([mk]b)$", value.lower());
            if m:
                num = int(m.group(1));
                if m.group(2) == "mb":
                    num = num * 1024 * 1024;
                elif m.group(2) == "kb":
                    num = num * 1024;
                else:
                    print "%s set logfile_maxbytes Error !!" % (section, value)
                    sys.exit(0);
                prog.logfile_maxbytes = num;
            else:
                sys.exit(0);
            
        elif name == "logfile_backups":
            prog.logfile_backups = int(value);
        else:
            print 'Unkonw property:"%s"' % (name)
    return prog;        
    
def create_all_programs(programs):
    global config;
    
    for section in config.sections():
        prefix_pos = section.find("program:");
        if prefix_pos == 0:
            pname = section[len("program:"):]
            
            if programs.has_key(pname):
                pass;
            else:
                prog = create_programs_with_section(section);
                prog.loger_init(prog.logfile_maxbytes, prog.logfile_backups);
                programs[pname] = prog;
        else:
            # not program section, skip
            pass;

def add_channel(name):
    '''return  0: OK
              -1: The program has existed.
    '''
    global programs, config, logger
    
    if programs.has_key(name):
        return  -1
    
    section_properties = [
        ('command', 'itvencoder --gst-debug=2 --gst-debug-no-color -c /etc/itvencoder/%s.conf' % (name)),
        ('autostart', 'false'),
        ('autorestart', 'true'),
        ('startsecs', '3'),
        ('logfile', '/var/log/iprocess/%s.log'  % (name)),
        ('logfile_maxbytes', '20MB'),
        ('logfile_backups', '10')
        ];
        
    sec_name = "program:%s" % (name);
    
    logger.info('Add channel sections ...');
    
    if sec_name in config.sections():
        return -1
    else:
        config.add_section(sec_name);
    
    logger.info('Set channel properties ...');
    
    for option, value in section_properties:
        logger.info('Set channel "%s" "%s"="%s"' % (sec_name, option, value));
        config.set(sec_name, option.strip(), value.strip())
    
    logger.info('Writing the setting ...');
    config.write(open(conf_filename, "w"));
    
    logger.info('Refresh the config ...');
    reload_config();
    
    return 0

def del_channel(name):
    '''return   0 : OK
               -1 : program not exist
    '''
    global programs, config
    
    if not programs.has_key(name):
        return -1
    
    sec_name = "program:%s" % (name);
    
    if sec_name in config.sections():
        logger.info("del channel %s" % (name))
        config.remove_section(sec_name)
        config.write(open(conf_filename, "w"));
        programs.pop(name);
    else:
        return -1
    
    
    reload_config();
    
    return 0

def reload_config():
    global config;
    
    config = ConfigParser.ConfigParser();
    config.read(conf_filename);
    
    create_all_programs(programs);
    

def get_programs():
    """return all CPrograms
    """
    global programs, config
    if len(programs) == 0:
        print "Sections:%s" % (config.sections());
        create_all_programs(programs);

    return programs;

def get_rpc_port():
    global rpc_port, config
    if rpc_port == 0:
        rpc_port = config.getint("rpc_server", "port");
    return rpc_port;

def get_http_port():
    global http_port, config
    if http_port == 0:
        http_port = config.getint("http_server", "port");
    return http_port;

def get_http_root():
    global http_root, config
    if http_root == "":
        http_root = config.get("http_server", "root");
    
    return http_root;

def get_log_dir():
    global logfile
    return os.path.dirname(logfile);

def get_proceessd_logfile():
    global logfile
    return logfile;



def get_programs_status_data():
    global programs, HTML_ROW
    
    logger = logging.getLogger("iprocessd.config")
    
    all_program_status = [];
    for (name, prog) in programs.items():
        logger.info("%s status=%s action=%s" % (prog.name, prog.status(), prog.action()));
        ps = {};
        ps['status'] = prog.status();
        ps['info'] = '';
        ps['name'] = prog.name;
        ps['action'] = prog.action();
        ps['live_time'] = '%s' % (prog.live_time());
        
        all_program_status.append(ps);
        
        
    
    return all_program_status;
