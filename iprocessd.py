#!/usr/bin/env python
#coding=utf-8

import os;
import sys
import time
import logging.handlers
import daemon
import processdconfig as pdc
from cprogramproxy import CProgramProxy
from cxmlrpcserver import CXMLRPCServer
# from iprocesshttp import iProcessHTTP
import flaskhttp as http


#################################################################################################################

version = "0.2.0"

#################################################################################################################

def usage():
    usage_info = '''\
    iprocessd -c [config-file]
        -h | --help:    Get help infomation.
        -v | --version: Get tool's current version.
        -c : Set the iprocess config-file.
        
     eg.
        iprocessd -c /etc/iprocessd.conf"
        
    ''';
    print usage_info;

#################################################################################################################



class iProcessd:
    LOG_FORMAT = logging.Formatter('[%(asctime)s-%(filename)s:%(lineno)s] %(message)s');
    def __init__(self):
        self.services = {};
        
        self.logger = logging.getLogger('iprocessd')
        log_handler = logging.handlers.RotatingFileHandler(
            pdc.get_proceessd_logfile(),
            maxBytes = 0x800000,
            backupCount = 10);
        log_handler.setFormatter(self.LOG_FORMAT);
        
        if daemon.IS_DAEMON == False:
            stream_handler = logging.StreamHandler()
            stream_handler.setFormatter(self.LOG_FORMAT);
            stream_handler.setLevel(logging.DEBUG)
            self.logger.addHandler(stream_handler);
        
        self.logger.addHandler(log_handler);
        self.logger.setLevel(logging.INFO);

    def __del__(self):
        programs = pdc.get_programs();
        for name, prog in programs.items():
            prog.quit = True;
            print "program:%s exit ." % (name)

#     def create_http_service(self, port):
#         http = iProcessHTTP(port);
#         
#         return http;
    
    def start_program_all(self, is_beginning):
        programs = pdc.get_programs();
        for (name, program) in programs.items():
            if(is_beginning and program.autostart == True):
                self.logger.info("Program '%s' starting......" % (name));
                program.start();
            
    def stop_program_all(self):
        programs = pdc.get_programs();
        for (name, program) in programs.items():
            self.logger.info("Program '%s' stopping......" % (name));
            program.stop();

    def start(self, is_beginning):
        self.logger.info('Creating XMLRPCService ...');
        self.services['XMLRPCService'] = CXMLRPCServer(port = pdc.get_rpc_port());
        self.services['XMLRPCService'].register_instance(CProgramProxy())
        
        self.logger.info('Creating WEBService ...');
        http.set_root_path(pdc.get_http_root());
        self.services['WEBService'] = http.iProcessHTTP(port = pdc.get_http_port());
        
        
#         port = pdc.get_http_port();
#         self.services['HTTPService']   = self.create_http_service(port)

        for name, service in self.services.items():
            self.logger.info('Start %s ...' % (name));
            service.start();
        
        self.logger.info('Start all-programs ...');
        self.start_program_all(is_beginning);

        return 0;

    def checkStatus(self):
        for (name, service) in self.services.items():
            if service.thread == None:
                self.logger.error('"%s" NOT created !!!' % (name));
            elif service.thread.isAlive() == False:
                self.logger.error("%s has DOWN, please check it." % (name));
            
        
        programs = pdc.get_programs();
        for (name, prog) in programs.items():
            self.logger.debug("Program %s check......", name);
            if prog.isAlive() == False:
                if prog.autorestart == True and prog.stopped_by_user == False:
                    self.logger.info("Service %s starting......" % (name));
                    prog.start();
            else:
                self.logger.debug("Program %s Running......", name);
    
    def stop(self):
        self.stop_program_all();
        
        for (name, server) in self.services.items():
            print "Shutdown %s ..." % (name)
            server.stop();
        
#         Daemon.stop(self);


if __name__ == "__main__":
    logger = logging.getLogger('iprocessd.master');
    
    pid = daemon.get_pid_from_file(daemon.DAEMON_PID_FILE);
    if pid != -1:
        print "IProcess already started."
        sys.exit(0);
        
    sys.path.append('/etc/iprocess/src');
    config_file = "";

    i = 1;
    while i < len(sys.argv):
        if sys.argv[i] in ('-h', '--help'):
            usage();
            sys.exit(0);
        elif sys.argv[i] in ('-v', '--version'):
            print 'iProcee current version:%s' % (version)
            sys.exit(0);
        elif sys.argv[i] == '-c':
            i += 1;
            config_file = sys.argv[i];
        elif sys.argv[i] == 'stop':
            daemon.shutdown();
            sys.exit(0);
        elif sys.argv[i] == 'kill':
            os.system("ps -ef | grep iprocessd.py | grep python | awk '{ print $2}' | xargs kill");
            sys.exit(0);
        else:
            print "Unknown args %s" % (sys.argv[i]);
        i += 1;
    
    if (config_file == ""):
        print "Must set -c config_file."
        exit(0);
    elif (os.path.exists(config_file) == False):
        print "config_file %s NOT exists!!" % (config_file)
        exit(0);
    
#     if os.getuid() != 0:
#         print "Permission denied, Please use root try again."
#         sys.exit(0);
    
    ###########################################
    logger.info('Initialize the iprocessd-conf "%s"' % (config_file));
    pdc.init(config_file);
    log_file = pdc.get_proceessd_logfile()
    
#     daemon.daemonize("/dev/null", log_file, log_file);
    
    logger.info('Create iProcessd ...')
    processd = iProcessd();
    logger.info('Start iProcessd ...')
    ret = processd.start(is_beginning = True);
    
    if ret >= 0:
#         daemon.write_pidfile();
        logger.info("iProcessd start SUCCESSFUL !!!")
        try:
            while (True):
                processd.checkStatus();
                time.sleep(0.4);
        except KeyboardInterrupt:
            pass;
    else:
        logger.info("iProcessd start FAILED !!!")
    
#     processd.stop();
#     daemon.shutdown();
    

