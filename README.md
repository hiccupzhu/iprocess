## Welcome to iProcess ##

**iProcess** is a Control System on linux platform

### Introduction ###

IProcess is management and it is based on python.
It is including a flexible log system, and it can log-rotate and set peer log file's max size.
Another hand, its web implemnt used "flask-python", so you can add any WEB-API for various request.


### Installing ###

    > cd .../iprocess
    > python ./setup.py install

- **itsdangerous** >= 0.24

    \[[web](https://pypi.python.org/pypi/itsdangerous/0.24)\]
    \[[download](https://pypi.python.org/packages/source/i/itsdangerous/itsdangerous-0.24.tar.gz#md5=a3d55aa79369aef5345c036a8a26307f)\] : Various helpers to pass trusted data to untrusted environments and back.

- **Werkzeug** >= 0.9.4

    \[[web](https://pypi.python.org/pypi/Werkzeug/0.9.4)\]
    \[[download](https://pypi.python.org/packages/source/W/Werkzeug/Werkzeug-0.9.4.tar.gz)\] : The Swiss Army knife of Python web development

- **Jinja2** >= 2.7.2

    \[[web](https://pypi.python.org/pypi/Jinja2/2.7.2)\]
    \[[download](https://pypi.python.org/packages/source/J/Jinja2/Jinja2-2.7.2.tar.gz)\] : A small but fast and easy to use stand-alone template engine written in pure python.

- **Flask** >= 0.10.1

    \[[web](https://pypi.python.org/pypi/Flask)\]
    \[[download](https://pypi.python.org/packages/source/F/Flask/Flask-0.10.1.tar.gz)\] : Flask is a microframework for Python based on Werkzeug, Jinja 2 and good intentions. 


### Running iProcess ###

    > sudo python iproessd -c /etc/iprocessd.ini

### Configuration File ###

- **[iprocessd]**
 - logfile : Set iprocessd's log file.
- **[rpc_server]**
 - port : Set RPC's litenning port
- **[http_server]**
 - username : Login user name
 - password : Login password
 - port : Set HTTP's litenning port
 - root : Set HTTP root directory
- **[program:xxx]**
 - autorestart = true
 - startsecs = 1
 - command = itvencoder --gst-debug-no-color --gst-debug=2 -c /etc/itvencoder/demo.conf
 - autostart = false
 - logfile = /var/log/iprocess/demo.log
 - logfile_maxbytes = 20MB
 - logfile_backups = 10

- **Configure Example**

	    [iprocessd]
	    logfile = /var/log/iprocess/iprocessd.log

		[program:demo]
		autorestart = true
		startsecs = 1
		command = subprocess
		autostart = false
		logfile = /var/log/iprocess/demo.log
		logfile_maxbytes = 20MB
		logfile_backups = 10
		
		[rpc_server]
		port = 1122
		
		[http_server]
		username = user
		password = 123
		root = /etc/itvencoder/www
		port = 8080


### Contact US###
Email : hiccupzhu@gmail.com