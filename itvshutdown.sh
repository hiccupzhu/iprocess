#!/bin/bash

#ps -ef | grep tsplay | grep B4U | awk '{print $2}' | xargs kill

PARENT_ID=`ps -ef | grep iprocess | grep python | awk '{print $2}'`

echo "iproceesd pid=$PARENT_ID"

ps -ef | grep $PARENT_ID | grep -v iprocessd | grep -v 'grep ' | grep -E '^\w+\s+[0-9]+\s+[0-9]+' | awk '{print $2}' | xargs kill
kill $PARENT_ID
rm -f /tmp/iprocessd.pid