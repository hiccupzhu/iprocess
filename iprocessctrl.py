#!/usr/bin/env python
#coding=utf-8

import os
import re
import sys
import xmlrpclib
import daemon


class iXMLRPCProxy:
    def __init__(self):
        self.server = xmlrpclib.ServerProxy("http://localhost:1122");
    
    def list(self):
        plist = self.server.list();
        for p in plist:
            print p;
    
    def status(self):
        status = self.server.status();
        for (name, st) in status.items():
            print "%s        %s" % (name, st)
    
    def start(self, name):
        print 'Start "%s" ... ' % (name)
        ret = self.server.start(name);
        if ret == 0:
            print '"%s" start successfully.' % (name)
        else:
            print '"%s" start failed.' % (name)
    
    def stop(self, name):
        self.server.stop(name);
    
    def shutdown(self):
        try:
            self.server.shutdown();
        except IOError, e:
            print "Error:", e;
        except OSError, e:
            print "Error:", e;
        except xmlrpclib.ProtocolError, e:
            while os.path.exists(daemon.DAEMON_PID_FILE):
                os.system('rm -f %s' % (daemon.DAEMON_PID_FILE));
            print "IProcessd has shutdown."
    
    def add_channel(self, name, pattern):
        if os.getuid() != 0:
            print 'Permission denied, Please use root try again.'
            os.sys.exit(0);
        m = re.match('''^([a-z]+?),ch=([0-9]+?)\{enc=([0-9]+?)\{v=([0-9]+?),a=([0-9]+?)(?:,s=([0-9]+?))?(?:\:([a-z]+?))?\}\}$''', pattern);
        if m:
            print "Creating the itvencoder's config file ..."
            ret = os.system('ichannel -o /etc/itvencoder/%s.conf -p "%s"' % (name, pattern));
            if ret == 0:
                ret = self.server.add_channel(name);
                if ret == -1:
                    print 'Channel "%s" has exist !!' % (name)
                else:
                    print 'Channel "%s" added !!' % (name)
            else:
                print 'Create "/etc/itvencoder/%s.conf" FAILED !!' % name
        else:
            print "The pattern format error."
        
        
    def del_channel(self, name):
        if os.getuid() != 0:
            print 'Permission denied, Please use root try again.'
            os.sys.exit(0);
        ret = self.server.del_channel(name);
        if ret == 0:
            print 'Channel "%s" deleted ' % (name)
        else:
            print 'Channel "%s" not exist ' % (name)
            os.sys.exit(0);
        if os.path.exists("/etc/itvencoder/%s.conf" % (name)):
            os.system("rm -f /etc/itvencoder/%s.conf" % (name));
        
def excute_commands(proxy, words):
    if (words[0] in ("ls", "list")):
        proxy.list();
    elif (words[0] in ("status")):
        proxy.status();
    elif (words[0] == "add"):
        print words;
        if len(words) != 3:
            print "Too few arguments."
            return -1;
        name = words[1];
        pattern = words[2];
        proxy.add_channel(name, pattern);
    elif (words[0] == "del"):
        name = words[1];
        proxy.del_channel(name);
    elif (words[0] == "start"):
        proxy.start(words[1]);
    elif (words[0] == "stop"):
        proxy.stop(words[1]);
    elif (words[0] == 'shutdown'):
        proxy.shutdown();
    elif (words[0] in ("q", "quit")):
        return -1;
    else:
        print "unknown command !!!";
    
    return 0;

if __name__ == "__main__":
    sys.path.append('/etc/iprocess/src');
    proxy = iXMLRPCProxy();
    
    if len(sys.argv) > 1:
        excute_commands(proxy, sys.argv[1:]);
        sys.exit(0);
    
    while True:
        values = raw_input(">");
        try:
            words = values.split();
            ret = excute_commands(proxy, words)
            if ret == -1:
                break; 
        except Exception as ex:
            print "exception", ex;
        
