import threading
import logging
import subprocess
import os
import time
import datetime
import programconfig as pgc

def onsignal_term(signum, frame):
    print "SIGTERM:", signum, "", frame

class CProgram():
    log_dir = '/var/log/iprocess';
    def __init__(self, pname):
        self.name = pname;
        self.command = "";
        self.autostart = False;
        self.autorestart = True;
        self.startsecs = 1;
        self.logfile = "%s/%s.log" % (self.log_dir, pname);
        self.logfile_maxbytes = 0x800000;
        self.logfile_backups = 10;
        self.quit = False;
        self.thread = None;
        self.stopped_by_user = True;
        
        self.log_format = logging.Formatter('[%(asctime)s-%(filename)s:%(lineno)s] %(message)s');
        self.logger = None;
        self.pipe = None;
        self.start_time = 0;
        
        '''Save the configure object
        '''
        self.config = None;
#         signal.signal(signal.isRunningSIGTERM, onsignal_term);
#         signal.signal(signal.SIGINT,  onsignal_term);

    def __del__(self):
        self.quit = True;
#         print "CProgram::__del__() leving"
        
    def __str__(self):
        info = '''\
                  name=%s
                  command=%s
                  autorestart=%s
                  logfile=%s
               ''' % (self.name, self.command, str(self.autorestart), self.logfile);
        return info;
    
    def set_command(self, cmd):
        logger = logging.getLogger('iprocessd.' + self.name);
        words = cmd.split(); 
        i = 0;
        while i < len(words):
            if words[i].lower() == '-c':
                i += 1;
                prog_conf_file = words[i];
                if os.path.exists(prog_conf_file) == False:
                    logger.error('Program "%s" config file "%s" NOT exists!!' % (self.name, prog_conf_file));
                    os.sys.exit(0);
                self.config = pgc.ProgramConfig(prog_conf_file);
                if self.config == None:
                    logger.warning('Program "%s" config create FAILED !!!' % (self.name));
                break;
            i += 1;
        self.command = cmd;
    
    def loger_init(self, max_bytes, backups):
        if self.logger != None:
            return;
        self.logger = logging.getLogger(self.name)
        log_handler = logging.handlers.RotatingFileHandler(
            self.logfile,
            maxBytes = max_bytes,
            backupCount = backups);
        log_handler.setFormatter(self.log_format);

        self.logger.addHandler(log_handler);
        self.logger.setLevel(logging.DEBUG);
        
    def run(self):
        self.logger.info("Stop program '%s' ..." % (self.name));
        while (self.quit == False and self.thread.isAlive() == True):
            log = self.pipe.stdout.readline();
            if log == "":
                break;
            self.logger.info(log.strip('\n'));
        
        self.logger.info("Program '%s' has stopped." % (self.name));
        
        self.pipe.wait();
        self.thread = None;
        self.start_time = 0;

    def start(self, start_by_user = False):
        if self.isAlive() == True:
            self.logger.warning("Program '%s' has started!" % (self.name));
            return;
        
        self.logger.info("Start program '%s' ..." % (self.name));
        self.pipe = subprocess.Popen(self.command.split(), stdin = subprocess.PIPE,
                             stdout = subprocess.PIPE,
                             stderr = subprocess.STDOUT,
                             shell = False);

        self.stopped_by_user = False;
        self.thread = threading.Thread(target=self.run)
        # while the master-program exit, this sub-process will exit too.
        self.thread.setDaemon(True);
        self.start_time = time.time();
        self.thread.start();
        
        self.logger.info("Start program '%s' successful." % (self.name));
#         threading.Thread.start(self);
    def isAlive(self):
        if self.thread:
            return self.thread.isAlive()
        else:
            return False;
    
    def stop(self, stop_by_user = False):
        self.logger.info("Stop program '%s' ..." % (self.name));
        if self.isAlive() and self.pipe:
            self.pipe.terminate();
            self.stopped_by_user = stop_by_user;
        
        
    def kill(self, stop_by_user = False):
        self.logger.info("Kill program '%s' ..." % (self.name));
        if self.isAlive() and self.pipe:
            self.pipe.kill();
            self.stopped_by_user = stop_by_user;
    
    def status(self):
        if self.isAlive():
            return "Running";
        else:
            return "Stopped"
        
    def action(self):
        if self.isAlive():
            return "Stop";
        else:
            return "Start"
    def live_time(self):
        ret = 0;
        if self.start_time == 0:
            ret = 0;
        else:
            ret = time.time() - self.start_time;
        
        return self.strftime(ret);
    
    def strftime(self, tm):
        tm = long(tm);
        d = tm / (24 * 60 * 60);
        tm %= (24 * 60 * 60);
        h = tm / (60 * 60);
        tm %= (60 * 60);
        m = tm / 60;
        tm %= 60;
        s = tm;
        
        return '%03d:%02d:%02d:%02d' % (d, h, m, s);
        
