#!/usr/bin/env python
#coding=utf-8

import os
import re
import json


def get_region_text_with_name(text, pattern):
    region_name = ""
    start = 0;
    end = 0;
    data = "";
    
    while True:
        mm = re.search('\s*(%s)\s*=\s*{\s*\n' % (pattern), text);
        if mm:
            region_name = mm.group(1);
            
            data, start, end = get_region_text(text[mm.start():]);
            
            return (region_name, data.strip(), start, end);
            
            #end+1 : skip the last '}'
            text = text[end+1:];
        else:
            break;
    
    return ("", 0, 0, "")


def get_region_text(text):
    rcount = 0;
    start = -1;
    end = len(text);
    i = 0;
    while i < len(text):
        if text[i] == '{':
            if start == -1:
                start = i;
            rcount += 1;
        elif text[i] == '}':
            rcount -= 1;
            end = i;
            if rcount == 0 :
                break;
        else:
            pass;
        i += 1;
        
    start += 1;
#         print "start=%d end=%d" % (start, end)
    return text[start:end], start, end;





class Channel:
    def __init__(self, name, text):
        self.id = "";
        self.name = name;
        self.server_text = "";
        self.server = {};
        self.sources = self.__get_sources(text);
        self.encoders = self.__get_encoders(text);
        
    def get_sources(self):
        return self.sources;
    
    def get_encoders(self):
        return self.encoders;
    
    def set_server_text(self, text):
        self.server_text = text;
        self.server = self.parse_variables(self.server_text);
    
    def __get_sources(self, text):
        sources = {};
        
        source_name, data, start, end = get_region_text_with_name(text, 'source');
        
        if source_name != "":
            sources[source_name] = (data, start, end);
        
        return sources;
    
    def __get_encoders(self, text):
        enc_name = "";
        encoders = {};
        
        name, text, start, end = get_region_text_with_name(text, 'encoders');
        
        while True:
            enc_name, data, start, end = get_region_text_with_name(text, 'encoder[0-9]+');
            
            if enc_name != "": 
                encoders[enc_name] = (data, start, end);
                    
                #end+1 : skip the last '}'
                text = text[end+1:];
            else:
                break;
        
        return encoders;
    
    def parse_variables(self, text):
        variables = [];
        
        for mm in re.finditer('<var +name="([\w\- ]+)" +type="([\w,\[\] ]+)" *>([\w/ .:]+)</var>', text):
            if mm:
                name, type, context = mm.groups();
                
                variables.append({'name':name, 'type':type, 'context':context});
#                 variables.append(name, type, context);
                text = text[mm.end():]
        
        return variables;
    
    def variable_jsonstring(self):
        vars = {};
        encoders = [];
        
        vars['server'] = self.parse_variables(self.server_text);
        
        for name, source in self.get_sources().items():
            vars[name]= self.parse_variables(source[0]);
        for name, enc in self.get_encoders().items():
            encoders.append(self.parse_variables(enc[0])); 
        
#         vars['source'] = sources;
        vars['encoders'] = encoders;
        
        json_string = json.dumps(vars);
        
        return json_string;
    
    def set_variable_jsonstring(self, json_string):
        info = json.loads(json_string, 'utf-8');
        data = "";
        
        server = info['server'];
#         print "*********server"
        for (name, context) in server.items():
            name = str(name)
            context = str(context)
#             print "    name=%s context=%s" % (name, context);
            
            self.server_text = self.var_replace(self.server_text, name, context);
                
                
        
#         print "*********source"
        source = info['source'];
        
        
        data, start, end = self.sources['source'];
        
        for (name, context) in source.items():
            name = str(name)
            context = str(context)
            
            data = self.var_replace(data, name, context);
            
            self.sources['source'] = (data, start, end);
            
        
        encoders = info['encoders'];
#         print "*********encoders:", len(encoders);
        for i in range(0, len(encoders)):
            enc_name = 'encoder' + str(i);
            encoder = encoders[i];
            
#             print "enc_name=", enc_name
            
            for (name, context) in encoder.items():
                name = str(name)
                context = str(context)

                data, start, end = self.encoders[enc_name];
                
                data= self.var_replace(data, name, context);
                
            self.encoders[enc_name] = (data, start, end);
#                 print "    name=%s context=%s" % (name, context);
            
        
        return 0;

    def var_replace(self, text, name, context):
        data = "";
        
        pattern = '''<var\s+name="%s"\s+type="([\w,\[\] ]+)"\s*>([\w/ .:]+)</var>''' % (name);
        
#         print 'pattern:', pattern
        m = re.search(pattern, text)
        if m:
            type, ctx = m.groups();
            repl = '''<var name="%s" type="%s">%s</var>''' % (name, type, context);
#             print 'repl:', repl
            data, num = re.subn(pattern, repl, text)
            
        return data;
    
    def dumps(self, filename):
        lines = [];
        
        lines.append("[server]\n");
        lines.append(self.server_text);
        lines.append('\n');
        lines.append('[channels]\n');
        lines.append('channel0 = {\n');
        lines.append('    source = {\n');
        data, start, end = self.sources['source'];
        lines.append(data);
        lines.append('    }\n');
        lines.append('    encoders = {\n')
        for i in range(0, len(self.encoders)):
            enc_name = 'encoder' + str(i);
            data, start, end = self.encoders[enc_name];
            lines.append('        %s = {' % (enc_name));
            lines.append(data);
            lines.append('        }\n');
        lines.append('    }\n');
        lines.append('}\n');
        
        file(filename, "w").write(''.join(lines));
        
        return 0;


class ProgramConfig():
    def __init__(self, conf_file):
        self.root = {};
        self.conf_file = conf_file;
        
        self.root = self.get_sections(conf_file);
        self.channels = self.get_channels(''.join(self.root['channels']));
        self.channel = self.channels[0];
        self.channel.set_server_text(''.join(self.root['server']));
    
    def get_sections(self, conf_file):
        lines = file(conf_file, "r").readlines();
        
        sections = {};
        sec_name = "";
        sec_text = [];
        for line in lines:
            text, num = re.subn("#.*", "", line)
            if text.strip() == "":
                continue;
            mm = re.match("^\[(\w+)\]$", text);
            if mm:
                if sec_name != "":
                    sections[sec_name] = sec_text;
                    sec_text = [];
                
                sec_name = mm.group(1);
            else:
                sec_text.append(text);
                
        if sec_name != "":
            sections[sec_name] = sec_text;
        
        
        return sections;
    
    def get_channels(self, text):
        chname = "";
        start = 0;
        end = 0;
        data = "";
        channels = [];
        
        while True:
            chname, data, start, end = get_region_text_with_name(text, 'channel[0-9]+');
            if chname != "":
                channels.append(Channel(chname, data));
                
                #end+1 : skip the last '}'
                text = text[end+1:]
            else:
                break;
        
        return channels;
    
    def variable_jsonstring(self):
        return self.channel.variable_jsonstring();
    
    def set_variable_jsonstring(self, jsonstring):
        ret = self.channel.set_variable_jsonstring(jsonstring);
        if ret == 0:
            self.channel.dumps(self.conf_file);
            self.__init__(self.conf_file);
        
        return 0;

        


if __name__ == "__main__":
    conf_file = './Star.conf';
    conf = ProgramConfig(conf_file);
    root = conf.root;
    lines = root['channels'];
    
    channels = conf.channels;
    
    
    json_string = '''{"server":{"streaming address":"0.0.10.0:20120"},"source":{"udpsrc uri":"udp://127.0.0.1:20000","deinterlace-method":"4","deinterlace-fields":"1","cachesize":"0x40000","video decoder":"h264dec","audio decoder":"mad","select audio0":"yes","select subtitle0":"yes"},"encoders":[{"width":"1024","height":"576","keep-aspect-ratio":"FALSE","bitrate":"1200","b frames":"3","profile":"high","threads":"12","select subtitle0":"yes","select audio0":"yes"}]}''';
    
    for channel in channels:
        print "********************", channel.name;
#         print ch
        print channel.set_variable_jsonstring(json_string)



    
    
    